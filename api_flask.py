#!/usr/bin/python3.6
# -*- coding: utf-8 -*-
# 
import sys
from flask import Flask, request
from flask import jsonify
from flask_cors import CORS
from flask_caching import Cache
from bson import json_util
import json
import codecs
import uuid
import soundfile as sf
import numpy as np
from io import open
import base64

from luis import SPEACH_TO_TEXT_MS
from yaco import YACO
from polly import POLLY


class YACO_API:
    def __init__(self, threshold):
        self.app = Flask(__name__)
        # Check Configuring Flask-Cache section for more details
        cache = Cache(self.app, config={'CACHE_TYPE': 'simple'})
        CORS(self.app)
        self.chatbot = YACO(threshold)     # chatbot 
        self.listen  = SPEACH_TO_TEXT_MS() # speach to text
        self.voice   = POLLY()             # text to speach

        self.chatbot.load_model()


        @cache.cached(timeout=60)
        @self.app.route("/get_transcription/", methods=['POST'])
        def get_transcription():
            peticion = request.get_json()
            audio_blob = peticion['audio']
            tmp_name = str(uuid.uuid4().hex) + ".wav"  # Genero un nombre unico random
            tmp_name = "./voice/" + tmp_name
            
            nchannels = 2
            sampwidth = 2
            framerate = 8000
            nframes = 100
            
            
            import wave
             
            # name = 'output.wav'
            audio_wav = wave.open(tmp_name, 'wb')
            audio_wav.setnchannels(nchannels)
            audio_wav.setsampwidth(sampwidth)
            audio_wav.setframerate(framerate)
            audio_wav.setnframes(nframes)
            
            audio_wav.writeframes(audio_blob)

            respuesta = self.listen.get_transcript(audio)
            data = ""
            message = ""

            if respuesta != "":
                respuesta_texto = respuesta['DisplayText'] 
                status = respuesta['RecognitionStatus']

                if status != 'Success':
                    message = "Transcripcion Erronea. Cod.Error 2001"
                    status = 'Error'

                data = {
                     "transcripcion": respuesta_texto
                    }
            else:
                status = 'Error'
                message = "Transcripcion Erronea. Cod.Error 2002"
            
            response =  {
                            "status": status,
                            "data": data,
                            "message": message
                        }
            
            return jsonify(response)


        @cache.cached(timeout=60)
        @self.app.route("/get_response_chatbot/", methods=['GET', 'POST'])
        def get_response_chatbot():
            print("inside get_response_chatbot")
           
            # variables del mensaje de salida
            status = ""
            message = ""
            data = ""

            id_respuesta_texto = "" # indica el indice de la respuesta, unico solo en conjunto con tag 
            respuesta_texto = ""
            tag = ""
            accuracy = ""

            # variables de entrada
            sentence = str(request.json['sentence'])
            id_user = str(request.json['id_user'])
            sentence = str(sentence).lower()

            print("sentence " + str(sentence))
            print("id_user " + str(id_user))

            try:
                respuesta = self.chatbot.response_chatbot(sentence, id_user)
                print("respuesta " + str(respuesta))

                # El id 
                id_respuesta_texto = respuesta[0]
                respuesta_texto = respuesta[1]
                tag  = respuesta[2]
                accuracy = respuesta[3]

                status = "success"
            except Exception as e:
                print(" Error al obtener respuesta " + str(e))
                status = "error"
                message = "Error al obtener la respuesta"
                data = null

            try:
                bytes_audio = self.voice.text_to_speech(respuesta_texto, tag + "_" + str(id_respuesta_texto))        
            except Exception as e:
                print("Error al generar audio. Error " + str(e))
                message = "Error al obtener el audio"
                bytes_audio = ""

            base64_audio = codecs.encode(bytes_audio, 'base64')
        
            data = {
                     "respuesta": respuesta_texto,
                     "tag": tag,
                     "accuracy": str(accuracy), 
                     "audio": str(base64_audio)  
                    }

            response =  {
                            "status": status,
                            "data": data,
                            "message": message
                        }
            
            return jsonify(response)

        @self.app.route("/do_training/", methods=['GET'])
        def do_training():
            print("inside do_training")

            # variables del mensaje de salida
            status = ""
            message = ""
            data = ""
            status_training = ""
            
            try:
                response = self.chatbot.train()
                resultado = "ok"
                status = "success"
                status_training = "success"
                data =  {
                    'status_training': status_training
                }  
            except Exception as e:
                print(" Error al entrenar " + str(e))
                status = "error"
                message = "Error durante el entrenamiento"

            response =  {
                "status": status,
                "data": data,
                "message": message
            }

            return jsonify(response)

if __name__ == "__main__":
    threshold = 0.4
    yaco_api = YACO_API(threshold)
    yaco_api.app.run(host='0.0.0.0', debug=False)   