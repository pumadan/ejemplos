#/usr/bin/python3
# -*- coding: utf-8 -*-

import sqlalchemy as db
import datetime

from sqlalchemy import Column, Integer, Text, MetaData, Table
from sqlalchemy import event
from sqlalchemy.schema import CreateSchema

# author @daniel.delangel
# version 1.0, jul 4, 2019 create


schema_name = "PACIENTES"
# creando conexion
engine = db.create_engine('mysql://root:[pasword]@127.0.0.1:3306/')
connection = engine.connect()
metadata = db.MetaData()
#paciente = db.Table('pacientes', metadata, autoload=True, autoload_with=engine)

connection.execute("CREATE DATABASE " + str(schema_name))
connection.close()

engine = db.create_engine('mysql://root:[pasword]@127.0.0.1:3306/' + str(schema_name))
connection = engine.connect()

# creando tablas
paciente = db.Table('pacientes', metadata,
                    Column('no_expediente', db.Integer, primary_key=True, autoincrement=False),
                    Column('fecha_ultima_consulta', db.DATE),
                    Column('tipo_sangre', db.NVARCHAR(3))     # FOR A+ AB- OR O+ etc.
                    )

alergia = db.Table('alergias', metadata,
                            Column('id', db.Integer, primary_key=True),
                            Column('no_expediente', db.Integer),
                            Column('nombre', db.Text),
                            Column('fecha_alta', db.DATE),
                            Column('medicamento', db.NVARCHAR(50))
)

# nota se debería crear una tabla de alergias y una más de alergias pacientes pero la desnormalice por simplicidad
metadata.create_all(engine)     # Creates the table

ultima_consulta1 = datetime.datetime(2019, 5, 15)
ultima_consulta2 = datetime.datetime(2019, 4, 14)
ultima_consulta3 = datetime.datetime(2019, 3, 13)

# Inserting many records at ones
query = db.insert(paciente)
values_list = [{'no_expediente': 1, 'fecha_ultima_consulta': ultima_consulta1, 'tipo_sangre': 'A+'},
               {'no_expediente': 2, 'fecha_ultima_consulta': ultima_consulta2, 'tipo_sangre': 'O+'},
               {'no_expediente': 3, 'fecha_ultima_consulta': ultima_consulta3, 'tipo_sangre': 'AB-'}]

ResultProxy = connection.execute(query, values_list)


query = db.insert(alergia)
values_list = [{'no_expediente': 1, 'nombre': 'Proteina leche', 'fecha_alta': ultima_consulta1, 'medicamento': 'Ibuprofeno'},
               {'no_expediente': 1, 'nombre': 'Acaros', 'fecha_alta': ultima_consulta1, 'medicamento': "Salbutamol"},
               {'no_expediente': 3, 'nombre': 'Trabajo', 'fecha_alta': ultima_consulta3, 'medicamento': "reposo"}]

ResultProxy = connection.execute(query, values_list)

print("Database and tables created")
model.py
Mostrando model.py.