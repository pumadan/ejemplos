import numpy as np
import pandas as pd
import time
import chardet
from sklearn.ensemble import RandomForestRegressor


# XXXX  for keras XXXXXXXXXXXXXXXXXX
from keras.models import Sequential
from keras.layers import Dense
# from sklearn.datasets.samples_generator import make_blobs
# from sklearn.preprocessing import MinMaxScaler

# XXXXXX for logistic_regression XXXXXXXXXXXXXX
from sklearn import linear_model
from sklearn import model_selection

# XXXXX For Metrics XXXXXXXXXXXXXXX
from sklearn.metrics import classification_report
from sklearn import metrics


'''
    dependencias:
    - python3
    - pip3

    - Pandas
    - numpy         
    - chardet
    - OpenBLAS
    - SciPy 
    - matplotlib
    - HDF5
    - Graphviz
    - pydot-ng
    - TensorFlow
    - keras
    - ggplot
    
    
    pip3 install pandas 
        
    -- numpy usualmente se instala con pandas pero si no
    pip3 install numpy
    sudo apt-get install build-essential cmake git unzip pkg-config libopenblas-dev liblapack-dev
    sudo apt-get install python-numpy python-scipy python-matplotlib python-yaml
    sudo pip3 install matplotlib
    sudo apt-get install libhdf5-serial-dev python-h5py
    sudo apt-get install graphviz
    sudo pip3 install pydot-ng
    sudo pip3 install tensorflow
    sudo pip3 install keras

    pip3 install ggplot
    
    se deben crear dos carpetas al nivel de este script 
    
    
    |-- train
    |-- test
    
    dentro de train colocar el archivo 
    
    datatraining.csv
    
    dentro de test colocar los archivos
    
    datatest.txt
    datatest2.txt
     
    NOTA: Los acentos en comentarios han sido omitidos intencionalmente debido al manejo de python de los mismos
'''


class PREDICTORS:
    def __init__(self):
        ''' Load a environment vars, and prepare data '''
        # XXXXXXXXXXXXXXXXXXX vars for random forest XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        self.data_training_file = './train/datatraining.csv'  # Nombre archivo con datos de entrenamiento
        self.data_test_file1 = './test/datatest.txt'          # Nombre archivo con datos de prueba 1
        self.data_test_file2 = './test/datatest2.txt'         # Nombre archivo con datos de prueba 2
        self.COLUMN_NAME_LABEL = 'Occupancy'

        self.labels_training = [] 
        self.features_training = []
        self.labels_test1 = [] 
        self.features_test1 = []
        self.labels_test2 = [] 
        self.features_test2 = []
        self.feature_list = []

        self.model_random_forest = None

        self.read_training_data()
        self.read_test_data()

        # get labels
        self.labels_training = self.get_labels(self.df_data_train, self.COLUMN_NAME_LABEL)
        self.labels_test1 = self.get_labels(self.df_data_test_file1, self.COLUMN_NAME_LABEL)
        self.labels_test2 = self.get_labels(self.df_data_test_file2, self.COLUMN_NAME_LABEL)

        # get features
        self.features_training = self.get_features(self.df_data_train, self.COLUMN_NAME_LABEL)
        self.features_test1 = self.get_features(self.df_data_test_file1, self.COLUMN_NAME_LABEL)
        self.features_test2 = self.get_features(self.df_data_test_file2, self.COLUMN_NAME_LABEL)

        # print("describe")
        # print(self.df_data_train.describe())
        # print(self.df_data_test_file1.describe())
        # print(self.df_data_test_file2.describe())

        # print(self.features_test1.shape)
        # print(self.features_test1.describe())

        # print(self.features_test2.shape)
        # print(self.features_test2.describe())

        self.MAE_rf_test1 = 0  # mean absolute error for random forest test1
        self.MAE_rf_test2 = 0  # mean absolute error for random forest test2

        self.MAE_nn_test1 = 0  # mean absolute error for neural network test1
        self.MAE_nn_test2 = 0  # mean absolute error for neural network test2

        self.MAE_lr_test1 = 0  # mean absolute error for logistic regression test1
        self.MAE_lr_test2 = 0  # mean absolute error for logistic regression test2

        # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

        # XXXXXXXXXXXXXXXXXXXXXXXXX  Vars for NN XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        self.model_nn = None
        # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

        # XXXXXXXXXXXXXXXXXXXXXXXXX  Vars for RL XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        self.model_logistic_regression = None
        # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    def find_encoding(self, fname):
        '''
         get encoding from origin file
        :param fname: data name file
        :return: charenc encoding del archivo
        '''
        r_file = open(fname, 'rb').read()
        result = chardet.detect(r_file)
        charenc = result['encoding']

        return charenc

    def read_training_data(self):
        ''' 
            load data training file, 
            encoding 
            show a short description
            get targets and labels  
        '''
        encoding_archivo_entrada = self.find_encoding(self.data_training_file)
        self.df_data_train = pd.read_csv(self.data_training_file, sep=",", encoding=encoding_archivo_entrada)
        # print(self.df_data_train.head())
        # print(self.df_data_train.shape)
        # print(self.df_data_train.describe())

        # print(self.df_data_train)
        
        # encoding data (no lo use porque expande mucho las columnas, y no es necesario para los datos )
        # self.df_data_train = pd.get_dummies(self.df_data_train)
        # print(self.df_data_train)

        # elimino la columna de fecha porque no es relevante para el analisis y puede meter ruido 
        self.df_data_train = self.df_data_train.drop('date', 1)
    
    def read_test_data(self):
        ''' load the twoo test data '''
        encoding_archivo_entrada = self.find_encoding(self.data_test_file1)
        self.df_data_test_file1 = pd.read_csv(self.data_test_file1, sep=",", encoding=encoding_archivo_entrada)

        encoding_archivo_entrada = self.find_encoding(self.data_test_file2)
        self.df_data_test_file2 = pd.read_csv(self.data_test_file2, sep=",", encoding=encoding_archivo_entrada)

        # elimino la columna de fecha porque no es relevante para el analisis y puede meter ruido 
        self.df_data_test_file1 = self.df_data_test_file1.drop('date', 1)
        self.df_data_test_file2 = self.df_data_test_file2.drop('date', 1)

    def get_labels(self, df, column_name):
        '''
        get labels from a df
        param df data frame
        param column name to be a target
        return labels np_array with the labels
        '''
        # Labels are the values we want to predict
        labels = np.array(df[column_name])
        return labels

    def get_features(self, df, column_name):
        ''' 
        get features from a df
        param df data frame
        param column_name name to be a target drop
        return features np_array with de features
        '''
        # Remove the labels from the features
        features = df.drop(column_name, axis = 1)
        self.feature_list = list(features.columns)
        features_np_array =  np.array(features)
        return features_np_array

    def random_forest_train(self):
        '''
        do a traing random forest algoritm
        '''
        # Instantiate model with 1000 decision trees
        self.model_random_forest = RandomForestRegressor(n_estimators = 1000, random_state = 42)

        # Train the model on training data
        print("Beginning training random forest")
        self.model_random_forest.fit(self.features_training, self.labels_training);
        print("ending training random forest")

    def random_forest_predict(self):
        # Use the forest's predict method on the test data
        predictions_test1 = self.model_random_forest.predict(self.features_test1)
        predictions_test2 = self.model_random_forest.predict(self.features_test2)

        print(predictions_test1)

        # Calculate the absolute errors
        errors_test1 = abs(predictions_test1 - self.labels_test1)
        errors_test2 = abs(predictions_test2 - self.labels_test2)
        
        # Print out the mean absolute error (mae)
        self.MAE_rf_test1 = round(np.mean(errors_test1), 2)
        self.MAE_rf_test2 = round(np.mean(errors_test2), 2)
        
        print('Mean Absolute Error random forest test1:' + str(self.MAE_rf_test1))  
        print('Mean Absolute Error random forest test2:' + str(self.MAE_rf_test2))

        predictions_test1_round = np.round(predictions_test1)
        predictions_test2_round = np.round(predictions_test2)

        print(predictions_test1_round)
        print("XXXXXXXXXXXXXXXXXXXXX")
        print(predictions_test2_round)

        print("Metricas test 1 \n")
        print(classification_report(self.labels_test1.tolist(), predictions_test1_round))

        print("\nMetricas test 2 \n")
        print(classification_report(self.labels_test2.tolist(), predictions_test2_round))

        self.get_ROC_curve(predictions_test1_round, self.labels_test1, "random forest test 1")
        self.get_ROC_curve(predictions_test2_round, self.labels_test2, "ramdom forest test 2")

    def neural_network_train(self):
        '''
        define a model for a nerural network
        train a nn 
        '''
        self.model_nn = Sequential()
        self.model_nn.add(Dense(4, input_dim=5, activation='relu'))
        self.model_nn.add(Dense(4, activation='relu'))
        self.model_nn.add(Dense(1, activation='sigmoid'))
        self.model_nn.compile(loss='binary_crossentropy', optimizer='adam')

        # training a model
        print("Beginning training neural network \n\n")
        self.model_nn.fit(self.features_training, self.labels_training, epochs=500, verbose=0)
        print("\n\nEnding training neural network")
        
    def neural_network_predict(self):
        # make a prediction
        predictions_test1 = self.model_nn.predict_classes(self.features_test1)
        predictions_test2 = self.model_nn.predict_classes(self.features_test2)

        errors_test1 = abs(predictions_test1 -  self.labels_test1)
        errors_test2 = abs(predictions_test2 -  self.labels_test2)

        # Print out the mean absolute error (mae)
        self.MAE_nn_test1 = round(np.mean(errors_test1), 2)
        self.MAE_nn_test2 = round(np.mean(errors_test2), 2)
        
        print('Mean Absolute Error neural network test1:' + str(self.MAE_nn_test1))  
        print('Mean Absolute Error neural network test2:' + str(self.MAE_nn_test2))

        print("Metricas test 1 \n")
        print(classification_report(self.labels_test1.tolist(), predictions_test1))

        print("Metricas test 2 \n")
        print(classification_report(self.labels_test2.tolist(), predictions_test2))

        self.get_ROC_curve(predictions_test1, self.labels_test1, "Neural Network test 1")
        self.get_ROC_curve(predictions_test2, self.labels_test2, "Neural Network test 2")

    def logistic_regression_train(self):
        self.model_logistic_regression = linear_model.LogisticRegression()

        print("Beginning training logistic regression \n\n")
        self.model_logistic_regression.fit(self.features_training, self.labels_training)
        print("Ending training logistic regression \n\n")

    def logistic_regression_predict(self):
        # make a prediction
        predictions_test1 = self.model_logistic_regression.predict(self.features_test1)
        predictions_test2 = self.model_logistic_regression.predict(self.features_test2)

        print("Prediciones 1")
        print(predictions_test1)

        errors_test1 = abs(predictions_test1 - self.labels_test1)
        errors_test2 = abs(predictions_test2 - self.labels_test2)

        # Print out the mean absolute error (mae)
        self.MAE_lr_test1 = round(np.mean(errors_test1), 2)
        self.MAE_lr_test2 = round(np.mean(errors_test2), 2)

        print('Mean Absolute Error logistic regression test1:' + str(self.MAE_lr_test1))
        print('Mean Absolute Error logistic regression test2:' + str(self.MAE_lr_test2))

        print("Metricas test 1 \n")
        print(classification_report(self.labels_test1.tolist(), predictions_test1))

        print("Metricas test 2 \n")
        print(classification_report(self.labels_test2.tolist(), predictions_test2))

        self.get_ROC_curve(predictions_test1, self.labels_test1, "Logistic Regression test 1")
        self.get_ROC_curve(predictions_test2, self.labels_test2, "Logistic Regression test 2")

    def get_ROC_curve(self, predictions, labels, name_model):
        df_roc = None
        fpr, tpr, _ = metrics.roc_curve(labels, predictions)
        df_roc = pd.DataFrame(dict(fpr=fpr, tpr=tpr))

        print("ROC " + name_model)
        print(df_roc)

        auc = metrics.auc(fpr, tpr)
        print("AUC " + name_model)
        print(auc)


if __name__ == '__main__':
    '''    
    encoding_archivo_entrada
    '''
    # XXXXXX LOAD DATA XXXXXXXXXXXXXXXXX
    # for execution time
    start_time = time.time()

    # inicializo condiciones
    predictors = PREDICTORS()

    # XXXXXXX  RANDOM FOREST XXXXXXXXXXX
    start_time_rf = time.time()
    # training_random_forest
    predictors.random_forest_train()

    # get predictions
    predictors.random_forest_predict()

    elapsed_time_rf = time.time() - start_time_rf
    print("Elapsed time random forest: %0.10f seconds." % elapsed_time_rf)

    # XXXXXXX  NEURAL NETWORK XXXXXXXXXXX
    start_time_nn  = time.time()
    # training_ neural network
    predictors.neural_network_train()

    # get predictions
    predictors.neural_network_predict()
    elapsed_time_nn = time.time() - start_time_nn
    print("Elapsed time neural networks: %0.10f seconds." % elapsed_time_nn)

    # XXXXXXX  LOGISTIC REGRESSION XXXXXXXXXXX
    start_time_lr = time.time()
    # training_logistic_regression
    predictors.logistic_regression_train()

    # get predictions
    predictors.logistic_regression_predict()
    elapsed_time_lr = time.time() - start_time_lr
    print("Elapsed time logistic regression: %0.10f seconds." % elapsed_time_lr)

    elapsed_time = time.time() - start_time
    print("Elapsed time: %0.10f seconds." % elapsed_time)
